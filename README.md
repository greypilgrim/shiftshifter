# shiftShifter

## Description
The purpose of this script is to match lesser-experienced people with more experienced people. Each time the script runs, the matches should be randomized.

## Usage
1. Modify the `people_sample.json` data file or create a new one.
   - If you change the name of the file, you will need to edit the script's input file name within the script itself.
   - The script does not use the `team` field but could be expanded by your invention.
2. Execute the `main.py` script, ie. `python3 main.py`

## License
                    GNU AFFERO GENERAL PUBLIC LICENSE
                       Version 3, 19 November 2007
