import json
import random

import jsonschema
from jsonschema import validate
import math

# This function will validate input data against our JSON schema
def validateJson(inputData):
    try:
        validate(instance=inputData, schema=schema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True

# Read in our JSON schema file as a JSON object
with open("schema.json", "r") as schema_file:
    schema = json.load(schema_file)

# Read in our input file as a JSON object
with open("people_sample.json", "r") as input_file:
    inputData = json.load(input_file)

peopleCount = len(inputData)

# Validate the input file against our schema
errorCount = 0
for key in inputData:
    isValid = validateJson(key)
    if not isValid:
        print(key)
        print("Given JSON data is invalid.")
        errorCount += 1
if errorCount == 0:
    print("All " + str(peopleCount) + " JSON entries are valid.")

# Divide the count of people by 2 rounded down to nearest whole number
cleanNumber = math.floor(peopleCount / 2)
print("The given number of people allows for " + str(cleanNumber) + " weeks of scheduling before recycling the list.")

# Sort by experience level from low to high
sortedList = sorted(inputData, key=lambda x: x["experience_level"])

# Make two lists of people names, one sorted low-to-high experience level and the other reversed
sortedListNames = []
sortedListNamesReversed = []
for key in sortedList:
    sortedListNames.append(key["name"])
    sortedListNamesReversed.insert(0, key["name"])

# Create a new first list and shorten it in case the total people count was uneven
shortSortedList = sortedListNames[0:cleanNumber]
# Remove the people in the first list from the second list
for key in shortSortedList:
    sortedListNamesReversed.remove(key)

# Shuffle the second list (more experienced people)
random.shuffle(sortedListNamesReversed)

# Pair people in the first list with the randomized order of people in the second list
print("Combo list")
for key in range(len(shortSortedList)):
    print(shortSortedList[key], sortedListNamesReversed[key])
